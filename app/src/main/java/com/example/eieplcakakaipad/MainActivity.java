package com.example.eieplcakakaipad;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends DriveService  {
    private static final String TAG = "MainActivity";

    private List<Product> availableProducts = new ArrayList<>();
    private List<Product> missingProducts=new ArrayList<>();
    private JSONObject jsonObj;

//    private String[] listNames = {"available","missing"};
    private String activeListName = "available"; //available or missing

    private ListView listView;
    Button list1bt, list2bt;
    FloatingActionButton confirmBt;
    ImageView load_logo;
    LinearLayout load_bg;

    int
            selectedBgColor, unSelectedBgColor,
            selectedTxtColor, unSelectedTxtColor;

    //notification
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    private List<Product> checkedProducts=new ArrayList<>();

    private CustomListAdapter adapter;


    private int lastExpiryLength=0;

    /*** CREATE **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        setupUI(findViewById(R.id.parent));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*CollapsingToolbarLayout toolBarLayout = findViewById(R.id.toolbar_layout);
        toolBarLayout.setTitle(getTitle());*/

        //init button and colors
        load_logo   = findViewById(R.id.loadingIv);
        load_bg     = findViewById(R.id.loadingLl);
        list1bt     = findViewById(R.id.list1bt);
        list2bt     = findViewById(R.id.list2bt);
        listView    = findViewById(R.id.listView);
        selectedBgColor     = this.getResources().getColor(R.color.dark);
        unSelectedBgColor   = this.getResources().getColor(R.color.pastel2);
        selectedTxtColor    = this.getResources().getColor(R.color.pastel2);
        unSelectedTxtColor  = this.getResources().getColor(R.color.dark);

        //test notification
        //AddReminder(new Product("POTATO","24/01/2021"));

        String content = "{\n" +
                "  \"missing\": [],\n" +
                "  \"available\": [\n" +
                "    {\n" +
                "      \"name\": \"TEST_Lait\",\n" +
                "      \"expiry\": \"14\\/05\\/2021\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"name\": \"TEST_Fraises\",\n" +
                "      \"expiry\": \"04\\/04\\/2020\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";


        Intent intent =getIntent();
        if(intent.hasExtra("DATA")){
            content = intent.getStringExtra("DATA");
        }


        try {
            setData(content.trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setListeners();
        switchList(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_scrolling, menu); //TODO : Add if needed
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /****** LISTENERS ****/
    void setListeners(){
        setAddButton();
        setConfirmButton();
        setRefreshButton();
        setListButton();

    }

    private void setAddButton() {
        FloatingActionButton addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(view -> AddProductPopup());
    }
    private void setConfirmButton() {
        confirmBt = findViewById(R.id.confirmButton);
        confirmBt.setOnClickListener(view -> ConfirmProductPopup());
    }
    private void setRefreshButton(){
        FloatingActionButton refreshButton = findViewById(R.id.refreshBt);
        //ImageButton refreshButton = findViewById(R.id.refreshBt);
        refreshButton.setOnClickListener(view -> refreshData());
    }
    private void setListButton(){
        list1bt.setOnClickListener(v -> {
            switchList(0);
        });
        list2bt.setOnClickListener(v -> {
            switchList(1);
        });
    }
/* NOT USED ANYMORE
    public void OnEditItem(View v){
        if(adapter != null) {
            adapter.setSelectedItem((int) v.getTag());
        }
        else{
            Log.w(TAG, "ADAPTER NULL but click on edit item");
        }
    }*/

    @SuppressLint("RestrictedApi")
    public void switchList(int list){

        activeListName = (list==0)?"available":"missing";
        setListData();

        list1bt.setBackgroundColor((list==0)?selectedBgColor:unSelectedBgColor);
        list1bt.setTextColor((list==0)?selectedTxtColor:unSelectedTxtColor);
        list2bt.setBackgroundColor((list==0)?unSelectedBgColor:selectedBgColor);
        list2bt.setTextColor((list==0)?unSelectedTxtColor:selectedTxtColor);

        confirmBt.setVisibility(
                (list == 1 && checkedProducts.size()>0)?
                        View.VISIBLE:View.GONE
        );

    }

    @SuppressLint("RestrictedApi")
    public void OnCheckProduct(View v){

        boolean checked = ((CheckBox) v).isChecked();
        int position    = (int)v.getTag();
        Product product = missingProducts.get(position);

        product.setChecked(checked);
        missingProducts.set(position,product);
        if(checked){
            if(checkedProducts.size()==0){
                confirmBt.setVisibility(View.VISIBLE); //TODO animation
            }
            checkedProducts.add(product);
        }
        else{
            checkedProducts.remove(product);
            if(checkedProducts.size()==0){
                confirmBt.setVisibility(View.GONE); //TODO animation
            }
        }
        Log.d(TAG,"on check products : "+checkedProducts.size());

        adapter.notifyDataSetChanged();

    }
    /******** DATA MANAGEMENT *******/

    public void AddProduct(Product product, String list_name) throws JSONException {

        boolean changed = true;
        if(list_name.matches("available")) {
            availableProducts.add(product);
            if(product.getExpiryDate().length()>0){
                AddReminder(product);
            }
        }
        else {
            if(Product.ContainsName(missingProducts,product) == -1){
                missingProducts.add(product);
            }
            else{
                changed = false;
            }

        }
        if(changed) {
            jsonObj.accumulate(list_name, product.getJsonObj());
            setListData();
            exportData();
        }
    }

    public void RemoveP(View v) {
        try {
            RemoveProduct((Product)v.getTag(),activeListName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void RemoveProduct(Product product, String list_name) throws JSONException {

        Log.d(TAG,"removing "+product.getProductName()+" from "+list_name);
        boolean availableList = list_name.matches("available");
        int index = Product.ContainsName(availableList?availableProducts:missingProducts,product);
        if(index > -1) {
            if(availableList) {
                availableProducts.remove(index);
                AddProduct(new Product(product.getProductName()), "missing");
            }
            else{
                missingProducts.remove(index);
            }
        }
        setListData();

        saveProducts(OnAvailableList()?availableProducts:missingProducts);

        /*if(!isTestUI()) {
            //TODO test it
            JSONArray arr = jsonObj.getJSONArray(list_name);
            JSONArray newArr = new JSONArray();

            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = (JSONObject) arr.get(i);
                if (obj.get("name") != product.getProductName()) {
                    newArr.put(product.getJsonObj());
                }
            }
            jsonObj.remove(list_name);
            jsonObj.put(list_name, newArr);

            exportData();
        }*/
    }

    // TODO loading animation + thread waiting for the sign in to finish (unless it already failed ?)
    private void refreshData() {
        if(isTestUI()){

            CreateNotification();

            return;
        }
        //load_logo.setVisibility(View.VISIBLE);
        load_bg.setVisibility(View.VISIBLE);
        StartLoadingAnim();
        Log.d(TAG,"REFRESHING DATA");
        if(isSignedIn()){
            ReadFile();
        }
        else{
            RequestSignIn();
        }

    }

    private void setListsFromJSON() throws JSONException {
        JSONArray array= jsonObj.getJSONArray("available");
        JSONObject obj;
        availableProducts.clear();
        missingProducts.clear();
        for(int i=0;i<array.length();i++)
        {
            obj= array.getJSONObject(i);
            if(obj.has("name") && obj.has("expiry")){
                availableProducts.add(
                        new Product(obj.getString("name"), obj.getString("expiry"))
                );
            }
        }
        array= jsonObj.getJSONArray("missing");
        for(int i=0;i<array.length();i++)
        {
            obj= array.getJSONObject(i);
            if(obj.has("name")){
                missingProducts.add(
                        new Product(obj.getString("name"))
                );
            }
        }
    }

    private void exportData() throws JSONException {
        String content = jsonObj.toString(2);
        SaveFile(content);
    }

    private void setListData(){

        if(OnAvailableList() && availableProducts.size()>0) {
            //sort according to expiry date
            SortProducts();
        }
        if(adapter == null){
            adapter = new CustomListAdapter(this, OnAvailableList()?availableProducts:missingProducts);
            listView.setAdapter(adapter);
        }
        else{
            adapter.SwitchList(OnAvailableList()?availableProducts:missingProducts, OnAvailableList());
        }
    }

    private void setData(String content) throws JSONException {
        jsonObj = new JSONObject(content);
        setListsFromJSON();
        setListData();
    }

    private void SortProducts(){
        Collections.sort(availableProducts, (obj1, obj2) -> {
            String e1 = obj1.getExpiryDate();
            String e2 = obj2.getExpiryDate();
            if (e1 == null || e1.length()==0 || e1.equals("")){
                return -1;
            }
            if (e2 == null || e1.length()==0|| e2.equals("")){
                return 1;
            }

            // ## Ascending order
            Date date1= null;
            try {
                date1 = new SimpleDateFormat("dd/MM/yyyy").parse(e1);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date date2= null;
            try {
                date2 = new SimpleDateFormat("dd/MM/yyyy").parse(e2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(date1 != null && date2 != null){
                Long obj3 = new Long(date1.getTime());
                Long obj4 = new Long(date2.getTime());
                return obj3.compareTo(obj4);
            }
            else{
                return 0;
            }
        });
    }

    /******** POP UPS *******/
    public void AddProductPopup(){

        final Dialog dialog;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_product_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final TextInputEditText tInput  = dialog.findViewById(R.id.expiryInput);
        final TextInputLayout tInputLayout  = dialog.findViewById(R.id.textInputLayout2);
        final AutoCompleteTextView nameEditText  = dialog.findViewById(R.id.nameInput);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                OnAvailableList()? Product.GetFoodNames() : Product.GetAllProductNames());
        nameEditText.setAdapter(adapter);
        if(!OnAvailableList()){
            tInputLayout.setVisibility(View.GONE);
//            tInput.setVisibility(View.GONE);
        }
        //expiry input listener
        //Auto complete date with slash
        tInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                //saveExpiry(holder.productExpiryDateView);
                int length = tInput.getText().length();
                boolean addSlash = false;
                //this check ensures that we are adding characters in the edit text (so that when we enter backspace, the slash won't be put-
                if(lastExpiryLength<length){
                    String txt = tInput.getText().toString();
                    int firstOcc = txt.indexOf('/');
                    int lastOcc = txt.lastIndexOf('/');
                    Log.d(TAG,"first occu :"+firstOcc+ "\nlast occ : "+lastOcc);
                    //if either two numbers have been entered and no slash is here yet
                    // or 5 characters are entered (date + / + month theoretically) and no more than one slash is present
                    // we add the next slash
                    addSlash = (length == 2 && firstOcc == -1)
                            ||
                            (length == 5 && firstOcc == lastOcc)
                    ;
                    if(addSlash){
                        tInput.setText(txt+"/");
                        tInput.setSelection(length+1);
                    }

                    if(length == 3 || length == 6) {
                        if(firstOcc == -1 && length == 6){
                            tInput.setText(
                                    txt.substring(0,2) + "/"+
                                            txt.substring(2,length)
                            );
                            tInput.setSelection(3);
                        }
                        else{
                            if(txt.charAt(length-1) != '/'){
                                tInput.setText(
                                        txt.substring(0,length-1) + "/"+
                                                txt.charAt(length-1)
                                );
                                tInput.setSelection(length+1);
                            }
                        }

                    }

                    //if 9 characters and less than 2 slashes add one 4 char before end
                    if(length == 9 && firstOcc == lastOcc){
                        tInput.setText(
                                txt.substring(0,5) + "/"+
                                        txt.substring(5,length)
                        );
                        tInput.setSelection(length+1);

                    }
                }

                lastExpiryLength = tInput.getText().length();



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {}

        });

        //ADD listener
        Button bt_add = dialog.findViewById(R.id.addButton);
        bt_add.setOnClickListener(v -> {
            //TextInputEditText nameEditText  = dialog.findViewById(R.id.nameInput);
            //TextInputEditText dateEditText  = dialog.findViewById(R.id.expiryInput);
            String productName              = nameEditText.getText().toString();
            Product newProduct              = null;
            if(OnAvailableList()){
                String productExpiryDate    = tInput.getText().toString();
                newProduct                  = new Product(productName, productExpiryDate);
                try {
                    newProduct = newProduct.CheckProductSyntaxe();
                } catch (ParseException e) {
                    e.printStackTrace();
                    newProduct = null;
                }
            }
            else{
                newProduct = new Product(productName);
            }
            if(newProduct!=null){
                try {
                    AddProduct(newProduct, activeListName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
            else{
                Toast.makeText(MainActivity.this, "WRONG SYNTAXE", Toast.LENGTH_LONG).show();
            }

        });

        dialog.show();


    }

    public void ConfirmProductPopup() {

        final Dialog dialog;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirm_available_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button bt_yes       = dialog.findViewById(R.id.yesButton);
        Button bt_cancel    = dialog.findViewById(R.id.cancelButton);
        bt_yes.setOnClickListener(v -> {
            //get all checked product
            //delete them from missing list
            missingProducts.removeAll(checkedProducts);
            //add them to available list
            availableProducts.addAll(checkedProducts);
            setListData();

            //modify json object consequently
            try {
                //removed products in missing list
                JSONArray newArr = new JSONArray();
                for (int i = 0; i < missingProducts.size(); i++) {
                    newArr.put(missingProducts.get(i).getJsonObj());
                }
                jsonObj.remove("missing");
                jsonObj.put("missing", newArr);

                //add products in available list
                for (Product p : checkedProducts
                ) {
                    jsonObj.accumulate("available", p.getJsonObj());
                }

                exportData();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "YES clicked");
            dialog.dismiss();

            //uncheck all boxes
            for (int i = 0; i < listView.getCount(); i++) {
                View mChild = listView.getChildAt(i);

                //Replace R.id.checkbox with the id of CheckBox in your layout
                CheckBox mCheckBox = (CheckBox) mChild.findViewById(R.id.checkbox_item);
                mCheckBox.setChecked(false);
                Product product = missingProducts.get(i);
                product.setChecked(false);
                missingProducts.set(i,product);
            }

            //clear checked product list
            checkedProducts.clear();
        });
        bt_cancel.setOnClickListener(v -> {
            Log.d(TAG, "Cancel clicked");
            dialog.dismiss();
        });

        dialog.show();
    }

    public void DisplayProductPopup(final Product product){
        final Dialog dialog;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.display_product_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView nameView  = dialog.findViewById(R.id.nameView);
        TextView dateView  = dialog.findViewById(R.id.dateView);
        nameView.setText(product.getProductName());
        String expiryDate = product.getExpiryDate();
        if(expiryDate==null){
            dateView.setVisibility(View.GONE);
        }
        else {
            dateView.setText(product.getExpiryDate());
        }

        dialog.show();

        Button bt_delete = dialog.findViewById(R.id.deleteButton);
        bt_delete.setOnClickListener(v -> {
            try {
                RemoveProduct(product, activeListName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        });
    }

    public static void hideSoftKeyboard(Activity activity) {
        if(activity.getCurrentFocus() != null){
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    /******** Drive Service Method Override *******/
    @Override
    public void OnReadFileSuccess(String fileName, String content) {
        Log.d(TAG, "File reading success");
        try {
            setData(content.trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //load_logo.setVisibility(View.GONE);
        load_bg.setVisibility(View.GONE);
    }

    @Override
    public void OnSignInSuccess(){
        ReadFile();
    }

    /******** ANIMATIONS **********/
    void StartLoadingAnim(){
        ObjectAnimator animator = ObjectAnimator.ofFloat(load_logo,"rotation",0f, 360f);
        animator.setDuration(2000);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.start();
    }

    /*** OTHER **/
    boolean OnAvailableList(){
        return activeListName.matches("available");
    }

    /*** REMINDERS **/
    // From https://developer.android.com/training/notify-user/build-notification
    // For Android 8.0 +, see https://stackoverflow.com/questions/45711925/failed-to-post-notification-on-channel-null-target-api-is-26
    // but not for now because notification will only appear on main app (tablet)
    // TODO add action https://stackoverflow.com/questions/48260875/android-how-to-create-a-notification-with-action/48261097
    void CreateNotification(){
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_foreground)
                .setContentTitle("CA VA POURRIR")
                .setContentText("LE LAIT")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(0, builder.build());
    }

    void CancelAlarm(){
        // If the alarm has been set, cancel it.
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }
    }

    //From https://github.com/vaibhavjain30699/Reminder-App/blob/master/app/src/main
    void AddReminder(Product p){
        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(p.getExpiryDate());

            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+1"));
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, -5);
            calendar.set(Calendar.MINUTE,47);
            calendar.set(Calendar.HOUR,20);
            calendar.set(Calendar.SECOND,0);
            Log.d(TAG,"alarm set to : "+calendar.getTime().toString());
            Intent intent = new Intent(MainActivity.this,NotifierAlarm.class);
            intent.putExtra("NAME",p.getProductName());
            intent.putExtra("RemindDate",p.getExpiryDate());
            intent.putExtra("id",0);
            PendingIntent intent1 = PendingIntent.getBroadcast(MainActivity.this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
            int ALARM_TYPE = AlarmManager.RTC_WAKEUP;
            alarmManager.set(ALARM_TYPE, calendar.getTimeInMillis(), intent1);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(ALARM_TYPE, calendar.getTimeInMillis(), intent1);
            } else {
                alarmManager.set(ALARM_TYPE, calendar.getTimeInMillis(), intent1);
            }*/

            Toast.makeText(MainActivity.this,"Inserted Successfully",Toast.LENGTH_SHORT).show();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void saveProducts(List<Product> products) throws JSONException {
        if(!isTestUI()) {
            //TODO test it : seems to work
            JSONArray newArr = new JSONArray();
            //convert product list to json array
            for (Product p : products
                 ) {
                newArr.put(p.getJsonObj());
            }
            jsonObj.remove(activeListName);
            jsonObj.put(activeListName, newArr);

            exportData();
        }
    }

    public void setProductList(List<Product> products) throws JSONException {
        if(OnAvailableList()){
            availableProducts = products;
        }
        else{
            missingProducts = products;
        }
        saveProducts(products);
    }

    //NOT USED
    /*//convert JSON content to list of products
    private List<Product> convertText(String content, String category) throws JSONException {
        List<Product> products  = new ArrayList<>();
        JSONObject root   = new JSONObject(content.trim());
        JSONArray array= root.getJSONArray(category);
        JSONObject obj;
        for(int i=0;i<array.length();i++)
        {
            obj= array.getJSONObject(i);
            if(obj.has("name")){
                if(obj.has("expiry")){
                    products.add(
                            new Product(obj.getString("name"), obj.getString("expiry"))
                    );
                }
                else {
                    products.add(
                            new Product(obj.getString("name"))
                    );
                }
            }

        }

        return products;
    }*/
}