package com.example.eieplcakakaipad;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class CustomListAdapter extends BaseAdapter {

    private static String TAG = "CustomListAdapter";
    private List<Product> listData;
    private LayoutInflater layoutInflater;
    private Context context;
    private int selected_position = -1;
    private boolean onAvailableList = true;
    private List<Integer> checkedProducts=new ArrayList<>();
    private InputMethodManager mgr;
    private boolean lastInputIsDelete = false;
    private int lastExpiryLength=0;



    public CustomListAdapter(Context aContext,  List<Product> listData) {
        this.context    = aContext;
        this.listData   = listData;
        layoutInflater  = LayoutInflater.from(aContext);
        this.mgr        = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    public boolean isEnabled(int position)
    {
        return true;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Product product = this.listData.get(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.productNameView = convertView.findViewById(R.id.textView3);
            holder.productExpiryDateView = convertView.findViewById(R.id.expiryInput);
            holder.backgroundView = convertView.findViewById(R.id.backgroundLl);
            holder.deleteBtView = convertView.findViewById(R.id.deleteBt);
            holder.editBtView = convertView.findViewById(R.id.editBt);
            holder.checkboxView = convertView.findViewById(R.id.checkbox_item);
            convertView.setTag(holder);

            holder.productExpiryDateView.setFocusableInTouchMode(true);

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.deleteBtView.setTag(product);
        holder.checkboxView.setTag(position);
        holder.editBtView.setTag(position);
        //Log.d(TAG,"setting position of product : "+holder.productNameView.getText().toString() + " to : "+position);
        holder.productExpiryDateView.setTag(position);
        holder.editBtView.setImageDrawable(context.getResources().getDrawable(android.R.drawable.ic_menu_edit));
        holder.editBtView.clearFocus();

        if(holder.productNameView != null && holder.productExpiryDateView != null && holder.backgroundView != null ) {

            holder.productNameView.setText(product.getProductName().toUpperCase());
            if(onAvailableList){
                holder.checkboxView.setVisibility(View.GONE);
                holder.editBtView.setVisibility(View.VISIBLE);
                holder.productExpiryDateView.setVisibility(View.VISIBLE);
                holder.productExpiryDateView.setTextColor(context.getResources().getColor(R.color.middle1));
                holder.productExpiryDateView.setText(product.getExpiryDate());

                //if the expiry date hasn't been entered yet, we let the text editable and the pic unchanged
                if(product.getExpiryDate().length()==0){
                    holder.productExpiryDateView.setEnabled(true);
                    holder.editBtView.setImageDrawable(context.getResources().getDrawable(android.R.drawable.ic_menu_save));
                }
                else {
                    holder.productExpiryDateView.setEnabled(false);
                    // check Products Expiry
                    if (product.isExpired()) {
                        holder.productExpiryDateView.setTextColor(context.getResources().getColor(R.color.middle2));
                        //holder.backgroundView.setBackground(context.getResources().getDrawable(R.drawable.expired_item_background));
                    }
                }

                //Edit listener
                holder.editBtView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setSelectedItem((int) v.getTag());
                    }
                });

            }
            else{

                // we are on missing list
                holder.editBtView.setVisibility(View.GONE);
                holder.checkboxView.setVisibility(View.VISIBLE);
                holder.productExpiryDateView.setEnabled(true);
                if(product.getExpiryDate().length()>0){
                    holder.productExpiryDateView.setText(product.getExpiryDate());
                    holder.productExpiryDateView.setVisibility(View.VISIBLE);
                }
                else{
                    holder.productExpiryDateView.setText("");
                    holder.productExpiryDateView.setVisibility(View.GONE);
                }
                if(product.isChecked()){
                    holder.checkboxView.setChecked(true);
                    holder.productExpiryDateView.setVisibility(View.VISIBLE);
                }
            }

            if(this.selected_position == position) //for available list
            {
                //switch to edit mode for this expiry date
                //holder.productExpiryDateView.setVisibility(View.VISIBLE);
                holder.productExpiryDateView.setEnabled(true);
                holder.productExpiryDateView.requestFocus();
                mgr.showSoftInput(holder.productExpiryDateView, InputMethodManager.SHOW_IMPLICIT);
                holder.editBtView.setImageDrawable(context.getResources().getDrawable(android.R.drawable.ic_menu_save));

                //save mode
                holder.editBtView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            saveExpiry(holder.productExpiryDateView);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            holder.productExpiryDateView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        try {
                            saveExpiry(holder.productExpiryDateView);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //disable edit mode for this input (as the edit is done)
                        holder.productExpiryDateView.setEnabled(false);
                        holder.editBtView.setImageDrawable(context.getResources().getDrawable(android.R.drawable.ic_menu_edit));
                    }
                    return false;
                }
            });
/*
            //save if last input was the backspace key
            holder.productExpiryDateView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                    Log.d(TAG,"key :  is ======== "+keyCode);
                    lastInputIsDelete = false;
                    if(keyCode == KeyEvent.KEYCODE_DEL) {
                        lastInputIsDelete = true;
                    }
                    return false;
                }
            });*/

            //Auto complete date with slash
            holder.productExpiryDateView.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                    //saveExpiry(holder.productExpiryDateView);
                    int length = holder.productExpiryDateView.getText().length();
                    boolean addSlash = false;
                    //this check ensures that we are adding characters in the edit text (so that when we enter backspace, the slash won't be put-
                    if(lastExpiryLength<length){
                        String txt = holder.productExpiryDateView.getText().toString();
                        int firstOcc = txt.indexOf('/');
                        int lastOcc = txt.lastIndexOf('/');
                        Log.d(TAG,"first occu :"+firstOcc+ "\nlast occ : "+lastOcc);
                        //if either two numbers have been entered and no slash is here yet
                        // or 5 characters are entered (date + / + month theoretically) and only one slash is present
                        // we add the next slash
                        addSlash = (length == 2 && firstOcc == -1)
                        ||
                                (length == 5 && firstOcc == lastOcc && firstOcc != -1)
                        ;
                        if(addSlash){
                            holder.productExpiryDateView.setText(txt+"/");
                            holder.productExpiryDateView.setSelection(length+1);
                        }

                        if(length == 3 || length == 6) {
                            //if 6 characters are there and no slash, then add one after 2 char
                            if(firstOcc == -1 && length == 6){
                                holder.productExpiryDateView.setText(
                                        txt.substring(0,2) + "/"+
                                                txt.substring(2,length)
                                );
                                holder.productExpiryDateView.setSelection(3);
                            }
                            else{
                                if(txt.charAt(length-1) != '/'){
                                    holder.productExpiryDateView.setText(
                                            txt.substring(0,length-1) + "/"+
                                                    txt.charAt(length-1)
                                    );
                                    holder.productExpiryDateView.setSelection(length+1);
                                }
                            }
                        }

                        //if 9 characters and less than 2 slashes add one 4 char before end
                        if(length == 9 && firstOcc == lastOcc){
                            holder.productExpiryDateView.setText(
                                    txt.substring(0,5) + "/"+
                                            txt.substring(5,length)
                                );
                                holder.productExpiryDateView.setSelection(length+1);

                        }
                    }

                    lastExpiryLength = holder.productExpiryDateView.getText().length();


                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {}

            });
        }
        else{
            Log.e(TAG, "One of the view is null");
        }


        return convertView;
    }

    static class ViewHolder {
        ImageButton deleteBtView;
        ImageButton editBtView;
        TextView productNameView;
        TextInputEditText productExpiryDateView;
        LinearLayout backgroundView;
        CheckBox checkboxView;
        //Product product;
    }

    public void setSelectedItem(int position)
    {
        //if not already selected
        if(this.selected_position != position)
        {
            this.selected_position  = position;
        }
        // else we unselect it, and we save the expiry date
        else
        {
            this.selected_position  = -1;
        }

        this.notifyDataSetChanged();
    }

    public void SwitchList(List<Product> list, boolean OnAvailableList){
        this.onAvailableList = OnAvailableList;
        this.listData        = list;
        notifyDataSetChanged();
    }

    public void saveExpiry(TextInputEditText et) throws JSONException {
        Log.d(TAG, "saving ***** : " );

        String text = et.getText().toString();
        int position    = (int)et.getTag();
        Product productTest = new Product("test",text);

        try {
            productTest = productTest.CheckProductSyntaxe();
        } catch (ParseException e) {
            e.printStackTrace();
            productTest = null;
        }

        //this means that the expiry date isn't correctly written
        if(productTest!=null) {
            Log.d(TAG,"product not null");

            Product product = listData.get(position);
            product.setExpiryDate(text);
            //listData.set(position, product);
            //Log.d(TAG, "product after : " + product.toString());

            if (context instanceof MainActivity) {
                ((MainActivity)context).setProductList(listData);
            }
        }
        else{
            Log.d(TAG,"product null");
            //the expiry date syntaxe is wrong so we keep the entry from before

            //listData.set(position, product);
            //we notify the user with a toast
            Toast.makeText(context, "WRONG SYNTAXE", Toast.LENGTH_LONG).show();
        }

        this.setSelectedItem(-1);
    }



};