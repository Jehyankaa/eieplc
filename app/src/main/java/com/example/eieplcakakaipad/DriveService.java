package com.example.eieplcakakaipad;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

import org.json.JSONException;
import java.util.Collections;

public class DriveService extends AppCompatActivity implements GestureDetector.OnGestureListener{

    private static final String TAG = "DriveService";

    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final int REQUEST_CODE_OPEN_DOCUMENT = 2;
    protected DriveServiceHelper mDriveServiceHelper;

    //private String DATA_FILEID = "1yXjVRt3ftGWJhrQvW8bTOJdHZ0-avTLt";
    //private static final String DATA_fileID = "1A3nBaYhAotdKHK-XeHdQzJsknb5KY_ny";
    private static final String DATA_fileID = "1qyKFk8lYXRyI1nskU0wYrjAhQKBvxNS1x-DkNPwFm04";

    private static final String DATA_folderID = "1bCYR1gUWQFDJyk1Of7mW4EfCzOu3xSdq";
    private String DATA_fileName;

    private boolean test_UI = false;

    //swipe
    private float x1, x2, y1, y2;
    private static int MIN_DISTANCE = 150;
    private GestureDetector gestureDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //init Gesture detector
        this.gestureDetector = new GestureDetector(DriveService.this, this);
    }
    @Override
    public void onStart(){
        super.onStart();
        if(!test_UI){
            RequestSignIn();
        }
    }

    public void OnSignInSuccess(){

    }

    public void OnReadFileSuccess(String fileName, String content){

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        Log.d(TAG, "ONACTIVITY RESULT requestcode = "+requestCode + "resultCode : "+ resultCode + "resultData : "+resultData);
        switch (requestCode) {
            case REQUEST_CODE_SIGN_IN:
                if (resultCode == Activity.RESULT_OK && resultData != null) {
                    handleSignInResult(resultData);
                }
                break;

            case REQUEST_CODE_OPEN_DOCUMENT:
                if (resultCode == Activity.RESULT_OK && resultData != null) {
                    Uri uri = resultData.getData();
                    if (uri != null) {
                        openFileFromFilePicker(uri);
                    }
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, resultData);
    }

    /**
     * Starts a sign-in activity using {@link #REQUEST_CODE_SIGN_IN}.
     */
    public void RequestSignIn() {
        Log.d(TAG, "Requesting sign-in");

        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(new Scope(DriveScopes.DRIVE_FILE))
                        .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this, signInOptions);

        // The result of the sign-in Intent is handled in onActivityResult.
        startActivityForResult(client.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }
    /**
     * Handles the {@code result} of a completed sign-in activity initiated from {@link
     * #RequestSignIn()}.
     */
    private void handleSignInResult(Intent result) {
        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(googleAccount -> {
                    Log.d(TAG, "Signed in as " + googleAccount.getEmail());

                    // Use the authenticated account to sign in to the Drive service.
                    GoogleAccountCredential credential =
                            GoogleAccountCredential.usingOAuth2(
                                    this, Collections.singleton(DriveScopes.DRIVE_FILE));
                    credential.setSelectedAccount(googleAccount.getAccount());
                    Drive googleDriveService =
                            new Drive.Builder(
                                    AndroidHttp.newCompatibleTransport(),
                                    new GsonFactory(),
                                    credential)
                                    .setApplicationName("Drive API Migration")
                                    .build();

                    // The DriveServiceHelper encapsulates all REST API and SAF functionality.
                    // Its instantiation is required before handling any onClick actions.
                    mDriveServiceHelper = new DriveServiceHelper(googleDriveService);
                    //ReadFile(); //might not be the right spot to do that ?
                    OnSignInSuccess();
                })
                .addOnFailureListener(exception -> Log.e(TAG, "Unable to sign in.", exception));
    }

    /**
     * Opens a file from its {@code uri} returned from the Storage Access Framework file picker
     */
    private void openFileFromFilePicker(Uri uri) {
        if (mDriveServiceHelper != null) {
            Log.d(TAG, "Opening " + uri.getPath());

            mDriveServiceHelper.openFileUsingStorageAccessFramework(getContentResolver(), uri)
                    .addOnSuccessListener(nameAndContent -> {
                        String name = nameAndContent.first;
                        String content = nameAndContent.second;

                        // mFileTitleEditText.setText(name);
                        // mDocContentEditText.setText(content);

                        // Files opened through SAF cannot be modified.
                        //setReadOnlyMode();
                    })
                    .addOnFailureListener(exception ->
                            Log.e(TAG, "Unable to open file from picker.", exception));
        }
    }


    /**
     * Retrieves the title and content of a file identified by {@code fileId} and populates the UI.
     */
    public void ReadFile() {
        if (mDriveServiceHelper != null) {
            Log.d(TAG, "Reading file " + DATA_fileID);

            mDriveServiceHelper.readFile(DATA_fileID)
                    .addOnSuccessListener(nameAndContent -> {
                        DATA_fileName = nameAndContent.first;
                        String content = nameAndContent.second;
                        OnReadFileSuccess(DATA_fileName, content);

                    })
                    .addOnFailureListener(exception ->
                            Log.e(TAG, "Couldn't read file.", exception));
        }
    }

    public void SaveFile(String fileContent) {
        if (mDriveServiceHelper != null && DATA_fileID != null) {
            Log.d(TAG, "Saving " + DATA_fileID);
            Log.d(TAG, "content " + fileContent);

            String fileName = DATA_fileName;

            mDriveServiceHelper.saveFile(DATA_fileID, fileName, fileContent)
                    .addOnFailureListener(exception ->
                            Log.e(TAG, "Unable to save file via REST.", exception));
        }
    }

    /**
     * Creates a new file via the Drive REST API.
     */
    public void CreateFile() {
        if (mDriveServiceHelper != null) {
            Log.d(TAG, "Creating a file.");

            mDriveServiceHelper.createFile(DATA_folderID)
                    .addOnSuccessListener(fileId -> ReadFile())
                    .addOnFailureListener(exception ->
                            Log.e(TAG, "Couldn't create file.", exception));
        }
    }

    boolean isSignedIn(){
        return (mDriveServiceHelper != null);
    }

    boolean isTestUI(){
        return test_UI;
    }

    /** Gesture Listeners override **/
    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Log.d(TAG, "event action 1 :  "+e1.getAction());
        Log.d(TAG, "event action 2 :  "+e2.getAction());

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        gestureDetector.onTouchEvent(event);
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();
                Log.d(TAG, "DOWN : x : "+x1 + " y : "+y1);
                break;

            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                y2 = event.getY();
                Log.d(TAG, "UP : x : "+x2 + " y : "+y2);

                float valueX = x2-x1;
                float valueY = y2-y1;

                if (Math.abs(valueX)>MIN_DISTANCE){
                    if(x2>x1)
                    {
                        //RIGHT SWIPE
                        Log.d(TAG, "RIGHT SWIIIIIIPE");
                    }
                    else{

                        Log.d(TAG, "LEFT SWIIIIIIPE");
                    }
                }
                else if (Math.abs(valueY)>MIN_DISTANCE){
                    if(y2>y1)
                    {
                        //RIGHT SWIPE
                        Log.d(TAG, "BOTTOM SWIIIIIIPE");
                    }
                    else{

                        Log.d(TAG, "TOP SWIIIIIIPE");
                    }
                }
                break;

        }
        return super.onTouchEvent(event);
    }
}