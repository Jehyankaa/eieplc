package com.example.eieplcakakaipad;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Product implements Serializable {
    private static final String TAG = "Product";

    private static final String[] FOODS = new String[]{
            "Pain","Kellogs", "Oeufs",
            "Biscuits", "Chips", "Huile", "Nutella",
            "Surimi", "Beurre", "Margarine", "Crème fraîche", "Yaourt", "Jambon", "Fromage","Tartare",
            "Sucre", "Farine", "Riz", "Spaghettis", "Tagliatelles", "Coquillettes",
            "Thé", "Café","Jus d'orange", "Lait", "Eau",
            "Pain burger", "Sauce burger", "Ketchup", "Mayonnaise", "Moutarde", "Sauce piquante", "Sauce soja",
            "Aubergine", "Patates", "Tomates", "Carottes", "Ail", "Oignon", "Poivrons", "Maïs",
            "Banane", "Clémentine", "Pomme", "Poire", "Oranges",
            "Sel","Sucre en morceaux", "Sucre en poudre",
            "Saucisses", "Fricadelles de poulet"
    };
    private static final String[] OTHERS = new String[]{
            "Dentifrice","Lessive", "Savon", "Gel douche", "Shampoing", "Shampoing L'Oréal", "Shampoing Head & Shoulders", "Après shampoing", "Ariel",
            "Cotons-tige", "Serviette", "Déodorant (Brut)", "Papier toilette", "Mouchoirs"
    };

    private String name;
    private String expiryDate;
    private boolean checked;

    public Product(String name, String expiryDate) {
        this.name       = name;
        this.expiryDate = expiryDate;
        this.checked    = false;
    }
    public Product(String name) {
        this.name       = name;
        this.expiryDate = "";
        this.checked    = false;
    }

    public String getProductName(){
        return this.name;
    }

    public String getExpiryDate(){
        return this.expiryDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", checked=" + checked +
                '}';
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public static int ContainsName(List<Product> products, Product product){

        String name = product.getProductName();
        int i = 0;
        for (Product p : products
             ) {
            if(p.getProductName().matches(name)){
                return i;
            }
            i++;
        }
        return -1;
    }


    public Product CheckProductSyntaxe() throws ParseException {
        String s = this.expiryDate;

        Log.d(TAG,"date est : "+s);
        //if nothing is entered as expiry date, just create the product with the name
        if(s.length()==0){
            return new Product(name);
        }

        //try to split with different delimiter (/, - or .)
        String[] array = s.split("/");
        if(array.length!=3){
            array = s.split("-");
            if(array.length!=3){
                array = s.split(".");
                if(array.length!=3){
                    return null;
                }
            }
        }

        String day = array[0];
        String month = array[1];
        String year = array[2];

        int d=Integer.parseInt(day);
        int m=Integer.parseInt(month);
        int y=Integer.parseInt(year);

        if( d==-1 || y==-1 || m==-1)
        {
            return null;
        }
        if(d>31 || m>12 || y<21 || y>9999){
            return null;
        }

        if(day.length()==1){
            day = "0"+day;
        }
        if(month.length()==1){
            month = "0"+month;
        }
        if(year.length()==2){
            year = "20"+year;
        }

        String newDate = day+'/'+month+'/'+year;
        Date date = null;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date==null){
            return null;
        }

        return new Product(name,newDate);
    }

    public boolean isExpired(){

        Date date= null;
        Date now = new Date();
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(expiryDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

        Long time1 = new Long(date.getTime());
        Long time2 = new Long(now.getTime());
        //Log.d(TAG, "TODAY : " + now.toString() + "\nEXPIRES : "+date.toString());

        return (time1<time2);

    }


    /** JSON Methods **/
    public JSONObject getJsonObj() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("expiry", this.expiryDate);
        obj.put("name", this.name);
        return obj;
    }
    public static JSONArray GetJsonArray(List<Product> products) throws JSONException {
        JSONArray out = new JSONArray();
        for (Product p : products) {
            out.put(p.getJsonObj());
        }
        return out;
    }

    /** AUTOCOMPLETE **/
    public static String[] GetFoodNames(){
        return FOODS;
    }
    public static String[] GetAllProductNames(){
        return JoinArrays(FOODS, OTHERS);
    }

    static String[] JoinArrays(String[] arr1, String[] arr2){
        List<String> both = new ArrayList<>();

        Collections.addAll(both,arr1);
        Collections.addAll(both,arr2);

        return both.toArray(new String[both.size()]);
    }
}
