package com.example.eieplcakakaipad;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.DriveScopes;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends DriveService {
    private static final String TAG = "SplashActivity";

    private ImageView border_iv, logo_iv;
    private long animDuration = 1500;
    private int frameDuration = 200;
    private GoogleAuthorizationCodeFlow flow;

    private void setupGoogleDriveApi() throws GeneralSecurityException, IOException {
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        List<String> scopes = new ArrayList<String>();
        scopes.add(DriveScopes.DRIVE);

        GoogleClientSecrets secrets = GoogleClientSecrets.load(
                jsonFactory, new InputStreamReader(getResources().openRawResource(R.raw.client_secret))
        );

        flow = new GoogleAuthorizationCodeFlow.Builder(
                transport, jsonFactory, secrets, scopes
        ).build();
    }

    private String getAuthorizationUrl() throws IOException {
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob:auto";
        AuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
        url.setRedirectUri(redirectUri);
        return url.build();
    }
    private void launchAuthorizationFlow() throws IOException {
        String authorizationUrl = getAuthorizationUrl();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(authorizationUrl));
        startActivity(browserIntent);
    }
    private String getAuthorizationCode() {
        Uri uri = getIntent().getData();
        if (uri != null && uri.toString().startsWith("urn:ietf:wg:oauth:2.0:oob")) {
            String code = uri.getQueryParameter("code");
            return code;
        }
        return null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        border_iv = (ImageView) findViewById(R.id.iv1);
        logo_iv = (ImageView) findViewById(R.id.iv2);

        StartLogoAnim();
        StartLoadingAnim();

        if(isTestUI()){
            final Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void OnSignInSuccess(){
        Log.d(TAG, "SIGN IN SUCCESS");
        String authorizationCode = getAuthorizationCode();
        if (authorizationCode != null) {
            try {
                GoogleTokenResponse response = flow.newTokenRequest(authorizationCode).execute();
                String accessToken = response.getAccessToken();
                // Use the access token to make authorized requests to the Google Drive API
            } catch (IOException e) {
                // Handle the error
            }
        }
        ReadFile();
    }

    @Override
    public void OnReadFileSuccess(String fileName, String content) {
        Log.d(TAG, "file reading success");

        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("DATA",content);
        startActivity(intent);
    }

    void StartLogoAnim(){
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.e),frameDuration);
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.i),frameDuration);
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.e),frameDuration);
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.p),frameDuration);
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.l),frameDuration);
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.c),frameDuration);
        animationDrawable.addFrame(getResources().getDrawable(R.drawable.logo_foreground),500);
        animationDrawable.setOneShot(true);
        logo_iv.setImageDrawable(animationDrawable);
        animationDrawable.start();
    }

    void StartLoadingAnim(){
        ObjectAnimator animator = ObjectAnimator.ofFloat(border_iv,"rotation",0f, 360f);
        animator.setDuration(animDuration);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator);
        animatorSet.start();
    }
}
